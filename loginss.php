<!DOCTYPE html>
<html>
<head>
<!-- 	// Albertus Hendrawan Widjaja - 160419009
	// Frederick Marco Linady - 160419030 -->
	<title></title>
	<link rel="stylesheet" type="text/css" href="main.css">

</head>
<body>
<div class="bg">
<?php
	if(isset($_GET['msg'])){
		if($_GET["msg"] == "gagal"){
			echo "<div id='tes'>Username dan Password tidak sesuai</div>";
		}

		if($_GET['msg'] == "gagalrole"){
			echo "<div id='tes'>Roles anda tidak diketahui. Contact ADMIN for further information</div>";
		}
	}

	if(isset($_SESSION['username']) && isset($_SESSION['type_user'])){
		header("Location:dashboard/octopus/index.php");
	}
?>

<form action="logincheck.php" method="POST">
		<div class="login-wrap">
			<div class="login-html">
				<input id="tab-1" type="radio" name="tab" class="sign-in" checked><label for="tab-1" class="tab">Sign In</label>
				<input id="tab-2" type="radio" name="tab" class="sign-up"><label for="tab-2" class="tab"></label>
				<div class="login-form">
						<div class="sign-in-htm">
							<div class="group">
								<label for="user" class="label">Username</label>
								<input id="user" type="text" class="input" name="username">
							</div>
							<div class="group">
								<label for="pass" class="label">Password</label>
								<input id="pass" type="password" class="input" data-type="password" name="passcode">
							</div>
							<div class="group">
							</div>
							<div class="group">
								<input type="submit" class="button" value="Sign In">
							</div>
							<div class="hr"></div>
							<div class="foot-lnk">
							</div>
						</div>
				</div>
			</div>
		</div>
	</form>
	</div>
</body>
</html>