-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2021 at 11:09 AM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pres_db_handler`
--

-- --------------------------------------------------------

--
-- Table structure for table `ambil_matakuliahs`
--

CREATE TABLE `ambil_matakuliahs` (
  `mahasiswas_id` int(11) NOT NULL,
  `matakuliahs_id` int(11) NOT NULL,
  `matakuliahs_buka_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jadwals`
--

CREATE TABLE `jadwals` (
  `id` int(11) NOT NULL,
  `hari` varchar(45) NOT NULL,
  `jam_mulai` varchar(45) NOT NULL,
  `jam_selesai` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_matakuliahs`
--

CREATE TABLE `jadwal_matakuliahs` (
  `matakuliahs_id` int(11) NOT NULL,
  `matakuliahs_buka_id` int(11) NOT NULL,
  `jadwals_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kehadirans`
--

CREATE TABLE `kehadirans` (
  `mahasiswas_id` int(11) NOT NULL,
  `matakuliahs_id` int(11) NOT NULL,
  `matakuliahs_buka_id` int(11) NOT NULL,
  `jadwals_id` int(11) NOT NULL,
  `jam` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswas`
--

CREATE TABLE `mahasiswas` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matakuliahs`
--

CREATE TABLE `matakuliahs` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matakuliahs_buka`
--

CREATE TABLE `matakuliahs_buka` (
  `id` int(11) NOT NULL,
  `kp` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `matakuliahs_kp`
--

CREATE TABLE `matakuliahs_kp` (
  `matakuliahs_id` int(11) NOT NULL,
  `matakuliahs_buka_id` int(11) NOT NULL,
  `kapasitas` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rules_kehadiran_kuliah`
--

CREATE TABLE `rules_kehadiran_kuliah` (
  `kehadirans_mahasiswas_id` int(11) NOT NULL,
  `kehadirans_matakuliahs_id` int(11) NOT NULL,
  `kehadirans_matakuliahs_buka_id` int(11) NOT NULL,
  `kehadirans_jadwals_id` int(11) NOT NULL,
  `jadwals_id` int(11) NOT NULL,
  `kode_unik` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ambil_matakuliahs`
--
ALTER TABLE `ambil_matakuliahs`
  ADD PRIMARY KEY (`mahasiswas_id`,`matakuliahs_id`,`matakuliahs_buka_id`),
  ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuli_idx` (`matakuliahs_id`,`matakuliahs_buka_id`),
  ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasisw_idx` (`mahasiswas_id`);

--
-- Indexes for table `jadwals`
--
ALTER TABLE `jadwals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_matakuliahs`
--
ALTER TABLE `jadwal_matakuliahs`
  ADD PRIMARY KEY (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
  ADD KEY `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1_idx` (`jadwals_id`),
  ADD KEY `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_idx` (`matakuliahs_id`,`matakuliahs_buka_id`);

--
-- Indexes for table `kehadirans`
--
ALTER TABLE `kehadirans`
  ADD PRIMARY KEY (`mahasiswas_id`,`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
  ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx` (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
  ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx1` (`mahasiswas_id`);

--
-- Indexes for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matakuliahs`
--
ALTER TABLE `matakuliahs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matakuliahs_buka`
--
ALTER TABLE `matakuliahs_buka`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `matakuliahs_kp`
--
ALTER TABLE `matakuliahs_kp`
  ADD PRIMARY KEY (`matakuliahs_id`,`matakuliahs_buka_id`),
  ADD KEY `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1_idx` (`matakuliahs_buka_id`),
  ADD KEY `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1_idx` (`matakuliahs_id`);

--
-- Indexes for table `rules_kehadiran_kuliah`
--
ALTER TABLE `rules_kehadiran_kuliah`
  ADD PRIMARY KEY (`kehadirans_mahasiswas_id`,`kehadirans_matakuliahs_id`,`kehadirans_matakuliahs_buka_id`,`kehadirans_jadwals_id`,`jadwals_id`),
  ADD KEY `fk_kehadirans_has_jadwals_jadwals1_idx` (`jadwals_id`),
  ADD KEY `fk_kehadirans_has_jadwals_kehadirans1_idx` (`kehadirans_mahasiswas_id`,`kehadirans_matakuliahs_id`,`kehadirans_matakuliahs_buka_id`,`kehadirans_jadwals_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwals`
--
ALTER TABLE `jadwals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jadwal_matakuliahs`
--
ALTER TABLE `jadwal_matakuliahs`
  MODIFY `matakuliahs_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mahasiswas`
--
ALTER TABLE `mahasiswas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `matakuliahs`
--
ALTER TABLE `matakuliahs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `matakuliahs_buka`
--
ALTER TABLE `matakuliahs_buka`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ambil_matakuliahs`
--
ALTER TABLE `ambil_matakuliahs`
  ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasiswas1` FOREIGN KEY (`mahasiswas_id`) REFERENCES `mahasiswas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuliah1` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`) REFERENCES `matakuliahs_kp` (`matakuliahs_id`, `matakuliahs_buka_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jadwal_matakuliahs`
--
ALTER TABLE `jadwal_matakuliahs`
  ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1` FOREIGN KEY (`jadwals_id`) REFERENCES `jadwals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_h1` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`) REFERENCES `matakuliahs_kp` (`matakuliahs_id`, `matakuliahs_buka_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kehadirans`
--
ALTER TABLE `kehadirans`
  ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal1` FOREIGN KEY (`mahasiswas_id`) REFERENCES `mahasiswas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal2` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`) REFERENCES `jadwal_matakuliahs` (`matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `matakuliahs_kp`
--
ALTER TABLE `matakuliahs_kp`
  ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1` FOREIGN KEY (`matakuliahs_id`) REFERENCES `matakuliahs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1` FOREIGN KEY (`matakuliahs_buka_id`) REFERENCES `matakuliahs_buka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `rules_kehadiran_kuliah`
--
ALTER TABLE `rules_kehadiran_kuliah`
  ADD CONSTRAINT `fk_kehadirans_has_jadwals_jadwals1` FOREIGN KEY (`jadwals_id`) REFERENCES `jadwals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kehadirans_has_jadwals_kehadirans1` FOREIGN KEY (`kehadirans_mahasiswas_id`,`kehadirans_matakuliahs_id`,`kehadirans_matakuliahs_buka_id`,`kehadirans_jadwals_id`) REFERENCES `kehadirans` (`mahasiswas_id`, `matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


-- -- phpMyAdmin SQL Dump
-- -- version 4.5.1
-- -- http://www.phpmyadmin.net
-- --
-- -- Host: 127.0.0.1
-- -- Generation Time: Mar 22, 2021 at 06:59 AM
-- -- Server version: 10.1.8-MariaDB
-- -- PHP Version: 5.6.14

-- SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
-- SET time_zone = "+00:00";


-- /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
-- /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
-- /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
-- /*!40101 SET NAMES utf8mb4 */;

-- --
-- -- Database: `test`
-- --

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `ambil_matakuliahs`
-- --

-- CREATE TABLE `ambil_matakuliahs` (
--   `mahasiswas_id` int(11) NOT NULL,
--   `matakuliahs_id` int(11) NOT NULL,
--   `matakuliahs_buka_id` int(11) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `jadwals`
-- --

-- CREATE TABLE `jadwals` (
--   `id` int(11) NOT NULL,
--   `hari` varchar(45) NOT NULL,
--   `jam_mulai` varchar(45) NOT NULL,
--   `jam_selesai` varchar(45) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `jadwal_matakuliahs`
-- --

-- CREATE TABLE `jadwal_matakuliahs` (
--   `matakuliahs_id` int(11) NOT NULL,
--   `matakuliahs_buka_id` int(11) NOT NULL,
--   `jadwals_id` int(11) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `kehadirans`
-- --

-- CREATE TABLE `kehadirans` (
--   `mahasiswas_id` int(11) NOT NULL,
--   `matakuliahs_id` int(11) NOT NULL,
--   `matakuliahs_buka_id` int(11) NOT NULL,
--   `jadwals_id` int(11) NOT NULL,
--   `tanggal` datetime DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `mahasiswas`
-- --

-- CREATE TABLE `mahasiswas` (
--   `id` int(11) NOT NULL,
--   `nama` varchar(45) DEFAULT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `matakuliahs`
-- --

-- CREATE TABLE `matakuliahs` (
--   `id` int(11) NOT NULL,
--   `nama` varchar(45) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `matakuliahs_buka`
-- --

-- CREATE TABLE `matakuliahs_buka` (
--   `id` int(11) NOT NULL,
--   `kp` varchar(45) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- -- --------------------------------------------------------

-- --
-- -- Table structure for table `matakuliahs_kp`
-- --

-- CREATE TABLE `matakuliahs_kp` (
--   `matakuliahs_id` int(11) NOT NULL,
--   `matakuliahs_buka_id` int(11) NOT NULL,
--   `kapasitas` varchar(45) NOT NULL
-- ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --
-- -- Indexes for dumped tables
-- --

-- --
-- -- Indexes for table `ambil_matakuliahs`
-- --
-- ALTER TABLE `ambil_matakuliahs`
--   ADD PRIMARY KEY (`mahasiswas_id`,`matakuliahs_id`,`matakuliahs_buka_id`),
--   ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuli_idx` (`matakuliahs_id`,`matakuliahs_buka_id`),
--   ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasisw_idx` (`mahasiswas_id`);

-- --
-- -- Indexes for table `jadwals`
-- --
-- ALTER TABLE `jadwals`
--   ADD PRIMARY KEY (`id`);

-- --
-- -- Indexes for table `jadwal_matakuliahs`
-- --
-- ALTER TABLE `jadwal_matakuliahs`
--   ADD PRIMARY KEY (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
--   ADD KEY `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1_idx` (`jadwals_id`),
--   ADD KEY `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_idx` (`matakuliahs_id`,`matakuliahs_buka_id`);

-- --
-- -- Indexes for table `kehadirans`
-- --
-- ALTER TABLE `kehadirans`
--   ADD PRIMARY KEY (`mahasiswas_id`,`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
--   ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx` (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`),
--   ADD KEY `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx1` (`mahasiswas_id`);

-- --
-- -- Indexes for table `mahasiswas`
-- --
-- ALTER TABLE `mahasiswas`
--   ADD PRIMARY KEY (`id`);

-- --
-- -- Indexes for table `matakuliahs`
-- --
-- ALTER TABLE `matakuliahs`
--   ADD PRIMARY KEY (`id`);

-- --
-- -- Indexes for table `matakuliahs_buka`
-- --
-- ALTER TABLE `matakuliahs_buka`
--   ADD PRIMARY KEY (`id`);

-- --
-- -- Indexes for table `matakuliahs_kp`
-- --
-- ALTER TABLE `matakuliahs_kp`
--   ADD PRIMARY KEY (`matakuliahs_id`,`matakuliahs_buka_id`),
--   ADD KEY `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1_idx` (`matakuliahs_buka_id`),
--   ADD KEY `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1_idx` (`matakuliahs_id`);

-- --
-- -- AUTO_INCREMENT for dumped tables
-- --

-- --
-- -- AUTO_INCREMENT for table `jadwals`
-- --
-- ALTER TABLE `jadwals`
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `jadwal_matakuliahs`
-- --
-- ALTER TABLE `jadwal_matakuliahs`
--   MODIFY `matakuliahs_id` int(11) NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `mahasiswas`
-- --
-- ALTER TABLE `mahasiswas`
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `matakuliahs`
-- --
-- ALTER TABLE `matakuliahs`
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- --
-- -- AUTO_INCREMENT for table `matakuliahs_buka`
-- --
-- ALTER TABLE `matakuliahs_buka`
--   MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
-- --
-- -- Constraints for dumped tables
-- --

-- --
-- -- Constraints for table `ambil_matakuliahs`
-- --
-- ALTER TABLE `ambil_matakuliahs`
--   ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasiswas1` FOREIGN KEY (`mahasiswas_id`) REFERENCES `mahasiswas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuliah1` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`) REFERENCES `matakuliahs_kp` (`matakuliahs_id`, `matakuliahs_buka_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Constraints for table `jadwal_matakuliahs`
-- --
-- ALTER TABLE `jadwal_matakuliahs`
--   ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1` FOREIGN KEY (`jadwals_id`) REFERENCES `jadwals` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_h1` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`) REFERENCES `matakuliahs_kp` (`matakuliahs_id`, `matakuliahs_buka_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Constraints for table `kehadirans`
-- --
-- ALTER TABLE `kehadirans`
--   ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal1` FOREIGN KEY (`mahasiswas_id`) REFERENCES `mahasiswas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal2` FOREIGN KEY (`matakuliahs_id`,`matakuliahs_buka_id`,`jadwals_id`) REFERENCES `jadwal_matakuliahs` (`matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

-- --
-- -- Constraints for table `matakuliahs_kp`
-- --
-- ALTER TABLE `matakuliahs_kp`
--   ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1` FOREIGN KEY (`matakuliahs_id`) REFERENCES `matakuliahs` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
--   ADD CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1` FOREIGN KEY (`matakuliahs_buka_id`) REFERENCES `matakuliahs_buka` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*-- MySQL Workbench Synchronization
-- Generated: 2021-03-21 00:40
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Daniel Soesanto

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE TABLE IF NOT EXISTS `mahasiswas` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(45) NOT NULL,
  `jurusanss_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_matakuliahs_jurusanss1_idx` (`jurusanss_id` ASC),
  CONSTRAINT `fk_matakuliahs_jurusanss1`
    FOREIGN KEY (`jurusanss_id`)
    REFERENCES `presensi_cloud`.`jurusanss` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `jadwals` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `hari` VARCHAR(45) NOT NULL,
  `jam_mulai` VARCHAR(45) NOT NULL,
  `jam_selesai` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs_buka` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `kp` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs_kp` (
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `kapasitas` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`matakuliahs_id`, `matakuliahs_buka_id`),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1_idx` (`matakuliahs_buka_id` ASC),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1_idx` (`matakuliahs_id` ASC),
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1`
    FOREIGN KEY (`matakuliahs_id`)
    REFERENCES `presensi_cloud`.`matakuliahs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1`
    FOREIGN KEY (`matakuliahs_buka_id`)
    REFERENCES `presensi_cloud`.`matakuliahs_buka` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `jadwal_matakuliahs` (
  `matakuliahs_id` INT(11) NOT NULL AUTO_INCREMENT,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `jadwals_id` INT(11) NOT NULL,
  PRIMARY KEY (`matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1_idx` (`jadwals_id` ASC),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC),
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_h1`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id`)
    REFERENCES `presensi_cloud`.`matakuliahs_kp` (`matakuliahs_id` , `matakuliahs_buka_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1`
    FOREIGN KEY (`jadwals_id`)
    REFERENCES `presensi_cloud`.`jadwals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ambil_matakuliahs` (
  `mahasiswas_id` INT(11) NOT NULL,
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  PRIMARY KEY (`mahasiswas_id`, `matakuliahs_id`, `matakuliahs_buka_id`),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuli_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasisw_idx` (`mahasiswas_id` ASC),
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasiswas1`
    FOREIGN KEY (`mahasiswas_id`)
    REFERENCES `presensi_cloud`.`mahasiswas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuliah1`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id`)
    REFERENCES `presensi_cloud`.`matakuliahs_kp` (`matakuliahs_id` , `matakuliahs_buka_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `kehadirans` (
  `mahasiswas_id` INT(11) NOT NULL,
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `jadwals_id` INT(11) NOT NULL,
  `tanggal` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`mahasiswas_id`, `matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC, `jadwals_id` ASC),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx1` (`mahasiswas_id` ASC),
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal1`
    FOREIGN KEY (`mahasiswas_id`)
    REFERENCES `presensi_cloud`.`mahasiswas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal2`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id` , `jadwals_id`)
    REFERENCES `presensi_cloud`.`jadwal_matakuliahs` (`matakuliahs_id` , `matakuliahs_buka_id` , `jadwals_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;*/


-- MySQL Workbench Synchronization
-- Generated: 2021-03-03 06:37
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Daniel Soesanto

/*SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `fakultass` (
  `id` INT(11) NOT NULL,
  `nama` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `jurusanss` (
  `id` INT(11) NOT NULL,
  `nama` VARCHAR(45) NOT NULL,
  `fakultass_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_jurusanss_fakultass1_idx` (`fakultass_id` ASC),
  CONSTRAINT `fk_jurusanss_fakultass1`
    FOREIGN KEY (`fakultass_id`)
    REFERENCES `fakultass` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `mahasiswas` (
  `id` INT(11) NOT NULL,
  `nama` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs` (
  `id` INT(11) NOT NULL,
  `nama` VARCHAR(45) NOT NULL,
  `jurusanss_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_matakuliahs_jurusanss1_idx` (`jurusanss_id` ASC),
  CONSTRAINT `fk_matakuliahs_jurusanss1`
    FOREIGN KEY (`jurusanss_id`)
    REFERENCES `jurusanss` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `jadwals` (
  `id` INT(11) NOT NULL,
  `hari` VARCHAR(45) NOT NULL,
  `jam_mulai` VARCHAR(45) NOT NULL,
  `jam_selesai` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs_buka` (
  `id` INT(11) NOT NULL,
  `kp` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `matakuliahs_kp` (
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `kapasitas` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`matakuliahs_id`, `matakuliahs_buka_id`),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1_idx` (`matakuliahs_buka_id` ASC),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1_idx` (`matakuliahs_id` ASC),
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs1`
    FOREIGN KEY (`matakuliahs_id`)
    REFERENCES `matakuliahs` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_matakuliahs_buka1`
    FOREIGN KEY (`matakuliahs_buka_id`)
    REFERENCES `matakuliahs_buka` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `jadwal_matakuliahs` (
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `jadwals_id` INT(11) NOT NULL,
  PRIMARY KEY (`matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1_idx` (`jadwals_id` ASC),
  INDEX `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC),
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_matakuliahs_h1`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id`)
    REFERENCES `matakuliahs_kp` (`matakuliahs_id` , `matakuliahs_buka_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_matakuliahs_has_matakuliahs_buka_has_jadwals_jadwals1`
    FOREIGN KEY (`jadwals_id`)
    REFERENCES `jadwals` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `ambil_matakuliahs` (
  `mahasiswas_id` INT(11) NOT NULL,
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  PRIMARY KEY (`mahasiswas_id`, `matakuliahs_id`, `matakuliahs_buka_id`),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuli_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasisw_idx` (`mahasiswas_id` ASC),
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_mahasiswas1`
    FOREIGN KEY (`mahasiswas_id`)
    REFERENCES `mahasiswas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_matakuliah1`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id`)
    REFERENCES `matakuliahs_kp` (`matakuliahs_id` , `matakuliahs_buka_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `kehadirans` (
  `mahasiswas_id` INT(11) NOT NULL,
  `matakuliahs_id` INT(11) NOT NULL,
  `matakuliahs_buka_id` INT(11) NOT NULL,
  `jadwals_id` INT(11) NOT NULL,
  `tanggal` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`mahasiswas_id`, `matakuliahs_id`, `matakuliahs_buka_id`, `jadwals_id`),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx` (`matakuliahs_id` ASC, `matakuliahs_buka_id` ASC, `jadwals_id` ASC),
  INDEX `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadw_idx1` (`mahasiswas_id` ASC),
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal1`
    FOREIGN KEY (`mahasiswas_id`)
    REFERENCES `mahasiswas` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mahasiswas_has_matakuliahs_has_matakuliahs_buka_has_jadwal2`
    FOREIGN KEY (`matakuliahs_id` , `matakuliahs_buka_id` , `jadwals_id`)
    REFERENCES `jadwal_matakuliahs` (`matakuliahs_id` , `matakuliahs_buka_id` , `jadwals_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;*/


