<?php 
include ('connectdb.php');
$mysqli = konek('localhost', 'root', '');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nama = $_POST['nama'];
    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    else {
        $mysqli->select_db('presensi_cloud');
        //simpan ke database
        $sql = "insert into universitass (nama) values ('".$nama."')";
        $result = $mysqli->query($sql);
        if ($result === TRUE) {
            //jika berhasil simpan ke database, maka selanjutnya ambil id yang tersimpan
            //dan buat schema dengan id tersebut
            $sql2 = "select id from universitass order by id desc limit 1";
            $result2 = $mysqli->query($sql2);
            $row = $result2->fetch_assoc();
            $id = $row['id'];
            //create new schema
            $newSchema = 'presensi_cloud_'.$id;
            $sql3 = "create database ".$newSchema;
            $result3 = $mysqli->query($sql3);

            $selectedTable = $_POST['tabel'];
            $customField = $_POST['custom'];
            $val = $_POST['values'];
            $panjang = $_POST['length'];
            $sql4 = "insert into metadatas (entity,custom_field,universitass_id) values ('".$selectedTable."','".$customField."','".$id."')";
            $resultss = $mysqli->query($sql4);

            $restore_file  = "master_schema.sql";
            $server_name   = "localhost";
            $username      = "root";
            $password      = "";

            $cmd = "mysql.exe -h {$server_name} -u {$username} {$newSchema} < $restore_file";
            exec($cmd);

            $mysqli->select_db($newSchema);
            $addCustomFields = "alter table ".$selectedTable." add ".$customField." ".$val." (".$panjang.")";
            $result4 = $mysqli->query($addCustomFields);

            header("Location:index.php");
            exit;
        }
        else {
            echo $mysqli->error;
        }
    }
}
?>