<?php
session_start();

include('connectdb.php');
include('dashboard/octopus/rsa-encrypt.php');
$mysqli = konek('localhost', 'root', '');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $nama = $_POST['username'];
    $pass = $_POST['passcode'];
    if ($mysqli->connect_errno) {
        printf("Connect failed: %s\n", $mysqli->connect_error);
        exit();
    }
    else {
        $mysqli->select_db('presensi_cloud');
        $kelas = new crypt();
        $encPass = $kelas->encrypRSA($pass);
        //baca isi data
        $sql = "select * from users where nama = '".$nama."' and password = '".$encPass."'";
        $result = $mysqli->query($sql);
        $row = $result->fetch_assoc();
	 	$jurusanId = $row['jurusanss_id'];
    	$userid = $row['idusers'];
    	$password = $row['password'];
    	$username = $row['nama'];

        if($encPass == $password && $nama == $username){
        	
        	$sqlGetRole = "select * from dac_roles where users_idusers = '".$userid."'";
        	$result2 = $mysqli->query($sqlGetRole);
        	$getSpecific = $result2->fetch_assoc();
        	$tipeUser = $getSpecific['type_user'];

        	if($tipeUser=="admin"){
        		$_SESSION['username'] = $nama;
        		$_SESSION['type_user'] = "admin";

        		header("Location:dashboard/octopus/index.php");
        	}
        	else if($tipeUser=="dekan" || $tipeUser=="wakil dekan" || $tipeUser=="manajer administrasi fakultas"){
        		$_SESSION['username'] = $nama;
        	   if($tipeUser=="wakil dekan"){
                    $_SESSION['type_user'] = "wakil dekan";
                }
                else if($tipeUser=="manajer administrasi fakultas"){
                    $_SESSION['type_user'] = "manajer administrasi fakultas";
                }
                else if($tipeUser=="dekan"){
                    $_SESSION['type_user'] = "dekan";
                }

        		header("Location:dashboard/octopus/index.php");
        	}
        	else if($tipeUser=="kepala jurusan" || $tipeUser=="paj" || $tipeUser=="dosen"){
        		$_SESSION['username'] = $nama;
                if ($tipeUser=="kepala jurusan") {
                    $_SESSION['type_user'] = "kajur";
                }
                else if($tipeUser=="paj"){
                    $_SESSION['type_user'] = "PAJ";
                }
                else if($tipeUser=="dosen"){
                    $_SESSION['type_user'] = "dosen";
                }
        		header("Location:dashboard/octopus/index.php");
        	}
        	else if($tipeUser=="Mahasiswa"){
        		$_SESSION['username'] = $nama;
        		$_SESSION['type_user'] = "mahasiswa";

        		header("Location:dashboard/octopus/index.php");
        	}
            else{ 
                header("Location:loginss.php?msg=gagalrole");
            }
        }
		else{
			header("Location:loginss.php?msg=gagal");
		}
    }
}
?>