<?php 
class crypt{
	public function encRSA($M) {
        $data[0]=1;
        for($j=0; $j<=35; $j++) {
            $rest[$j] = pow($M,1)%119;
            if($data[$j]>119) {
                $data[$j+1]=$data[$j]*$rest[$j]%119;
            } 
            else {
                $data[$j+1]=$data[$j]*$rest[$j];
            }
        }
        $get=$data[35]%119;
        return $get;
    }

    public function decRSA($E) {
        $data[0]=1;
        for($k=0; $k<=11; $k++) {
            $rest[$k] = pow($E,1)%119;
            if($data[$k]>119) {
                $data[$k+1]=$data[$k]*$rest[$k]%119;
            } 
            else {
                $data[$k+1]=$data[$k]*$rest[$k];
            }
        }
        $get=$data[11]%119;
        return $get;
    } 
    //encrypt
    public function encrypRSA($kalimat) {   
        $enc = "";  
        for($i=0;$i<strlen($kalimat);$i++)
        {            
            $m=ord($kalimat[$i]);            
            if($m<=119){
                $enc = $enc.chr($this->encRSA($m));
            }
            else
            {
                $enc = $enc.$kalimat[$i];
            }
        }
        return $enc;
    }

    //decrypt
    public function decrypRSA($kalimat) {
        $dec = "";
        $enc = $kalimat;                
        for($l=0;$l<strlen($kalimat);$l++)
        {
            $m=ord($enc[$l]);            
            if($m<=119){
                $dec = $dec.chr($this -> decRSA($m));
            }
            else
            {
                $dec = $dec.$enc[$l];
            }
        }
        return $dec;
    }
}
    
 ?>