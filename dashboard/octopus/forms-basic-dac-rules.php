<?php 
session_start();

if(!isset($_SESSION['username'])){
	header("location: loginss.php");
}
?>
<style type="text/css">
	#error-handler{
		background-color: white;
		color: red;
		text-align: center;
	}
	#win-handler{
		background-color: green;
		color:white;
		text-align: center;
	}
</style>
<!DOCTYPE html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Form DAC Rules</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="logo">
						<img src="assets/images/logo.png" height="35" alt="Porto Admin" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
					<span class="separator"></span>
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<?php
								echo "<span class='name' style='text-transform: capitalize;'>".$_SESSION['username']."</span>";
								echo "<span class='role' style='text-transform: capitalize;'>".$_SESSION['type_user']."</span>";
							?>
							</div>
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-signin.html"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			</form>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li>
										<a href="index.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									
									<li class="nav-parent nav-expanded nav-active">
										<a>
											<i class="fa fa-list-alt" aria-hidden="true"></i>
											<span>Add Forms</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="forms-basic-fakultas.php">
													 Fakultas
												</a>
											</li>	
											<li >
												<a href="forms-basic-jurusan.php">
													 Jurusan
												</a>
											</li>
											<li class="nav-active">
												<a href="forms-basic-dac-rules.php">
													 DAC Rules
												</a>
											</li>
											<li>
												<a href="forms-basic-dac-roles.php">
													 DAC Roles
												</a>
											</li>	
											<li>
												<a href="forms-basic-users.php">
													 Users
												</a>
											</li>

										</ul>
											<li class="nav-parent">
										<a>
											<i class="fa fa-list-alt" aria-hidden="true"></i>
											<span>Edit</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="forms-basic-edit-fakultas.php">
													 Fakultas
												</a>
											</li>
											<li>
												<a href="forms-basic-edit-jurusan.php">
													 Jurusan
												</a>
											</li>
											<li>
												<a href="forms-basic-edit-dac-rules.php">
													 DAC Rules
												</a>
											</li>
											<li>
												<a href="forms-basic-edit-dac-roles.php">
													 DAC Roles
												</a>
											</li>
											<li>
												<a href="forms-basic-edit-users.php">
													 Users
												</a>
											</li>
										</ul>
									</li>
									</li>
								</ul>
							</nav>								
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Forms DAC Rules</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Forms</span></li>
								<li><span>DAC Rules</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->
					<?php
					if(isset($_GET['msg'])){
						if($_GET["msg"] == "gagal1"){
								echo "<div id='error-handler'>Nama field atau panjang text masih kosong</div>";
						}
						else if($_GET['msg'] == "gagal2"){
							echo "<div id='error-handler'>Terjadi kesalahan</div>";
						}
						else{
								echo "<div id='win-handler'>Tambah DAC Rules berhasil</div>";
						}
					}
					?>										
					<div class="row">
						<div class="col-lg-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>
					
									<h2 class="panel-title">Tambah DAC Rules</h2>
								</header>
								<div class="panel-body">
									<form action="tambah-dac-rules.php" class="form-horizontal form-bordered" method="POST">
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault" >Operator</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="operator">
											</div>
										
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Value</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="valuee">
											</div>
											
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">DAC Path</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="dacpathh">
											</div>
											
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Kolom Field</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="kolomfield">
											</div>
											<span class="input-group-btn">
												<input type="submit" class="btn btn-success" value="Add">
												</span>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div>

						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Current Date</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
							</div>
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>
	</body>
</html>