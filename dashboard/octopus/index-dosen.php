<?php
session_start();
if(!isset($_SESSION['username'])){
	header("location: loginss.php");
}
?>
<!DOCTYPE html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Dashboard | JSOFT Themes | JSOFT-Admin</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="JSOFT Admin - Responsive HTML5 Template">
		<meta name="author" content="JSOFT.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/jquery-ui/css/ui-lightness/jquery-ui-1.10.4.custom.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="assets/vendor/morris/morris.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="index-dosen.php" class="logo">
						<img src="assets/images/logo.png" height="35" alt="JSOFT Admin" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">	
					<span class="separator"></span>
			
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@JSOFT.com">
								<?php
								echo "<span class='name' style='text-transform: capitalize;'>".$_SESSION['username']."</span>";
								echo "<span class='role' style='text-transform: capitalize;'>".$_SESSION['type_user']."</span>";
							?>
							</div>
			
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="pages-signin.html"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li class="nav-active">
										<a href="index-dosen.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-list-alt" aria-hidden="true"></i>
											<span>Display</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="forms-basic-display-mahasiswa.php">
													 Mahasiswa
												</a>
											</li>
											<li>
												<a href="forms-basic-display-jadwal.php">
													 Jadwal
												</a>
											</li>
											<li>
												<a href="forms-basic-display-kehadiranMHS.php">
													 Kehadiran Mahasiswa
												</a>
											</li>
										</ul>
									</li>	
								</ul>
							</nav>
				
							<hr class="separator" />
				
							<hr class="separator" />
				
							
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Dashboard</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index-dosen.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Dashboard</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>


					<!-- start: page -->	
					<div class="row">
						<div class="col-lg-12">
							<section class="panel panel-transparent">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>

									<h2 class="panel-title">My Profile</h2>
								</header>
								<div class="panel-body">
									<section class="panel panel-group">
										<header class="panel-heading bg-primary">

											<div class="widget-profile-info">
												<div class="profile-picture">
													<img src="assets/images/!logged-user.jpg">
												</div>
												<div class="profile-info">
													<?php
														echo "<span class='role' style='text-transform: capitalize;'>".$_SESSION['username']."</span>";
														echo "<h5 class='role' style='text-transform: capitalize;'>".$_SESSION['type_user']."</h5>";
													?>
													<div class="profile-footer">
														<span>Selamat Datang di Presensi Cloud! <?php
														echo "<span class='role' style='text-transform: capitalize;color:#810000;font-weight: bold'>".$_SESSION['username']."</span>"; ?></span>
													</div>
													
												</div>
											</div>

										</header>
										
									</section>

								</div>
							</section>
						</div>
						
					</div>

					<div class="row">
						<div class="col-md-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>
									<h2 class="panel-title">Absensi Dosen</h2>
								</header>
								<?php											  
										    //set it to writable location, a place for temp generated PNG files
										    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
										    
										    //html PNG location prefix
										    $PNG_WEB_DIR = 'temp/';

										    include "../phpqrcode/qrlib.php";    
										    
										    //ofcourse we need rights to create temp dir
										    if (!file_exists($PNG_TEMP_DIR))
										        mkdir($PNG_TEMP_DIR);
										    
										    
										    $filename = $PNG_TEMP_DIR.'test.png';
										    
										    //processing form input
										    //remember to sanitize user input in real-life solution !!!
										    $errorCorrectionLevel = 'H';
										    if (isset($_REQUEST['level']) && in_array($_REQUEST['level'], array('L','M','Q','H')))
										        // $errorCorrectionLevel = '$_REQUEST['level']';    
										        $errorCorrectionLevel = 'H';

										    $matrixPointSize = 10;
										    if (isset($_REQUEST['size']))
										        // $matrixPointSize = min(max((int)$_REQUEST['size'], 1), 10);
										    	$matrixPointSize = 10;

										    if (isset($_REQUEST['data'])) { 
										    
										        //it's very important!
										        if (trim($_REQUEST['data']) == '')
										            die('data cannot be empty! <a href="?">back</a>');
										            
										        // user data
										        $filename = $PNG_TEMP_DIR.'test'.md5($_REQUEST['data'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';
										        QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
										        
										    } else {    
										    
										        // //default data
										        // echo 'You can provide data in GET parameter: <a href="?data=like_that">like that</a><hr/>';    
										        // QRcode::png('PHP QR Code :)', $filename, $errorCorrectionLevel, $matrixPointSize, 2);    
										        
										    }  
								echo '<div class="panel-body">
									<form action="index-dosen.php" class="form-horizontal form-bordered" method="POST">
										<div class="form-group">
											<label class="col-md-3 control-label">Kode Spesial</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="data">
											</div>
										</div>
										
										<div class="form-group">
										
											<label class="col-md-3 control-label" for="inputSuccess">Pilih Matakuliah</label>
											<div class="col-md-6">
												<select class="form-control mb-md" name="jurusan">';
													
											            $con=mysqli_connect("localhost","root","","presensi_cloud_".$_SESSION['idjurusan']);
											            // Check connection
											            if (mysqli_connect_errno())
											            {
											            echo "Failed to connect to MySQL: " . mysqli_connect_error();
											            }

											            $result = mysqli_query($con,"SELECT * FROM matakuliahs");

											            while($fak = mysqli_fetch_array($result)){
											            echo "<option>".$fak['nama']."</option>";
											            }
											        
												echo '</select>	
												</form>
											</div>
												<span class="input-group-btn">
													<input type="submit" class="btn btn-success" value="Add" name="add">
												</span>
												</div>
												</section>
											</div>
										</div>';
										if(isset($_POST['add'])){
											echo '<div class="row">		
											<div class="form-group">
										 	
												<div class="col-md-12">
												<section class="panel">
													<header class="panel-heading">
														<div class="panel-actions">
															<a href="#" class="fa fa-caret-down"></a>
															<a href="#" class="fa fa-times"></a>
														</div>

														<h2 class="panel-title" style="color: green">QR CODE GENERATED</h2>
													</header>
													<div class="panel-body">
														<div class="owl-carousel">
															<div class="item">
																<img alt="" class="img-responsive" src="'.$PNG_WEB_DIR.basename($filename).'">
															</div>
														</div>
													</div>
												</section>
												</div>
											</div>
											</div>';	  
										}
										
										        
										    //config form
										    // echo '<form action="index-dosen.php" method="post">
										    //  echo' Data:&nbsp;<input name="data" value="'.(isset($_REQUEST['data'])?htmlspecialchars($_REQUEST['data']):'PHP QR Code :)').'" />&nbsp;
										    //     ECC:&nbsp;<select name="level">
										    //         <option value="L"'.(($errorCorrectionLevel=='L')?' selected':'').'>L - smallest</option>
										    //         <option value="M"'.(($errorCorrectionLevel=='M')?' selected':'').'>M</option>
										    //         <option value="Q"'.(($errorCorrectionLevel=='Q')?' selected':'').'>Q</option>
										    //         <option value="H"'.(($errorCorrectionLevel=='H')?' selected':'').'>H - best</option>
										    //     </select>&nbsp;
										    //     Size:&nbsp;<select name="size">';
										        
										    // for($i=1;$i<=10;$i++)
										    //     echo '<option value="'.$i.'"'.(($matrixPointSize==$i)?' selected':'').'>'.$i.'</option>';
										        
										    // echo '</select>&nbsp;
										    //     <input type="submit" value="GENERATE"><hr/>';

										         //display generated file
										    // echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" /><hr/>'; 
										    
										        
										    // // benchmark
										    // QRtools::timeBenchmark();    
											?>
								
							
						</div>
				</div>
				</section>
					<!-- end: page -->
				
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Current Date</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
							</div>		
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
		<script src="assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
		<script src="assets/vendor/jquery-appear/jquery.appear.js"></script>
		<script src="assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
		<script src="assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
		<script src="assets/vendor/flot/jquery.flot.js"></script>
		<script src="assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
		<script src="assets/vendor/flot/jquery.flot.pie.js"></script>
		<script src="assets/vendor/flot/jquery.flot.categories.js"></script>
		<script src="assets/vendor/flot/jquery.flot.resize.js"></script>
		<script src="assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
		<script src="assets/vendor/raphael/raphael.js"></script>
		<script src="assets/vendor/morris/morris.js"></script>
		<script src="assets/vendor/gauge/gauge.js"></script>
		<script src="assets/vendor/snap-svg/snap.svg.js"></script>
		<script src="assets/vendor/liquid-meter/liquid.meter.js"></script>
		<script src="assets/vendor/jqvmap/jquery.vmap.js"></script>
		<script src="assets/vendor/jqvmap/data/jquery.vmap.sampledata.js"></script>
		<script src="assets/vendor/jqvmap/maps/jquery.vmap.world.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js"></script>
		<script src="assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>


		<!-- Examples -->
		<script src="assets/javascripts/dashboard/examples.dashboard.js"></script>
	</body>
</html>