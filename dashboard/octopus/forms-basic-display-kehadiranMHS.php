<?php 
session_start();

if(!isset($_SESSION['username'])){
	header("location: loginss.php");
}
?>
<!DOCTYPE html>
<html class="fixed">
	<head>

		<!-- Basic -->
		<meta charset="UTF-8">

		<title>Form Display Kehadiran Mahasiswa</title>
		<meta name="keywords" content="HTML5 Admin Template" />
		<meta name="description" content="Porto Admin - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.css" />
		<link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.css" />
		<link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css" />
		<link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/css/datepicker3.css" />

		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css" />

		<!-- Theme CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme.css" />

		<!-- Skin CSS -->
		<link rel="stylesheet" href="assets/stylesheets/skins/default.css" />

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="assets/stylesheets/theme-custom.css">

		<!-- Head Libs -->
		<script src="assets/vendor/modernizr/modernizr.js"></script>

	</head>
	<body>
		<section class="body">

			<!-- start: header -->
			
			<header class="header">
				<div class="logo-container">
					<a href="index.php" class="logo">
						<img src="assets/images/logo.png" height="35" alt="Porto Admin" />
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			
				<!-- start: search & user box -->
				<div class="header-right">
					<span class="separator"></span>
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<figure class="profile-picture">
								<img src="assets/images/!logged-user.jpg" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
							</figure>
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<?php
								echo "<span class='name' style='text-transform: capitalize;'>".$_SESSION['username']."</span>";
								echo "<span class='role' style='text-transform: capitalize;'>".$_SESSION['type_user']."</span>";
							?>
							</div>
							<i class="fa custom-caret"></i>
						</a>
			
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="logout.php"><i class="fa fa-power-off"></i> Logout</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			</form>
			<!-- end: header -->

			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						<div class="sidebar-title">
							Navigation
						</div>
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li>
										<a href="index.php">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Dashboard</span>
										</a>
									</li>
									
									<li class="nav-parent nav-expanded nav-active">
										<a>
											<i class="fa fa-list-alt" aria-hidden="true"></i>
											<span>Display</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<a href="forms-basic-display-fakultas.php">
													 Fakultas
												</a>
											</li>	
											<li>
												<a href="forms-basic-display-jurusan.php">
													 Jurusan
												</a>
											</li>
											<li>
												<a href="forms-basic-display-mahasiswa.php">
													 Mahasiswa
												</a>
											</li>
											<li>
												<a href="forms-basic-display-jadwal.php">
													 Jadwal
												</a>
											</li>
											<li class="nav-active">
												<a href="forms-basic-display-kehadiranMHS.php">
													 Kehadiran Mahasiswa
												</a>
											</li>		
										</ul>
									</li>
								</ul>
							</nav>								
						</div>
				
					</div>
				
				</aside>
				<!-- end: sidebar -->

				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Forms Display Kehadiran Mahasiswa</h2>
					
						<div class="right-wrapper pull-right">
							<ol class="breadcrumbs">
								<li>
									<a href="index.php">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>Display</span></li>
								<li><span>Kehadiran Mahasiswa</span></li>
							</ol>
					
							<a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
						</div>
					</header>

					<!-- start: page -->	


					<div class="row">
						<div class="col-lg-12">
							<section class="panel">
								<header class="panel-heading">
									<div class="panel-actions">
										<a href="#" class="fa fa-caret-down"></a>
										<a href="#" class="fa fa-times"></a>
									</div>
									
									<h2 class="panel-title">Display Kehadiran Mahasiswa</h2>
									<div class="panel-body">
									<form action="forms-basic-display-kehadiranMHS.php" class="form-horizontal form-bordered" method="POST">
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Jurusan</label>
											<div class="col-md-6">
												<select class="form-control mb-md" name="namajurusan">
												<?php
											            $con=mysqli_connect("localhost","root","","presensi_cloud");
											            // Check connection
											            if (mysqli_connect_errno())
											            {
											            echo "Failed to connect to MySQL: " . mysqli_connect_error();
											            }

											            $result = mysqli_query($con,"SELECT DISTINCT nama FROM jurusanss");

											            while($fak = mysqli_fetch_array($result)){
											            echo "<option>".$fak['nama']."</option>";
											            }
											        ?>
											    </select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Filter</label>
											<div class="col-md-6">
												<select class="form-control mb-md" name="hakakses">
												<?php
											            $con=mysqli_connect("localhost","root","","presensi_cloud");
											            // Check connection
											            if (mysqli_connect_errno())
											            {
											            echo "Failed to connect to MySQL: " . mysqli_connect_error();
											            }
											            $namalogin = $_SESSION['username'];
											            $preres = mysqli_query($con,"SELECT idusers FROM users where nama = '".$namalogin."'");
											            $row = $preres->fetch_assoc();
														$selid = $row['idusers'];

											            $result = mysqli_query($con,"SELECT dac_rules.operator AS operator ,dac_rules.value AS valuess ,dac_rules.kolom_field AS kolomField FROM dac_rules INNER JOIN dac_roles ON dac_rules.id = dac_roles.dac_rules_id WHERE dac_roles.users_idusers = ".$selid." AND dac_rules.dac_path = '1x2' OR dac_rules.dac_path = '1x3' OR dac_rules.dac_path = 'ALL'");
											            while($fak = mysqli_fetch_array($result)){
											            echo "<option>".$fak['kolomField']." ".$fak['operator']." ".$fak['valuess']."</option>";
											            }
											        ?>
											    </select>
											</div>
											<span class="input-group-btn">
												<input type="submit" class="btn btn-success" value="Show">
											</span>
										</div>
										

								</header>
								<div class="panel-body">
											<?php
											if(isset($_POST['namajurusan']) && isset($_POST['hakakses']))
											{
												$con=mysqli_connect("localhost","root","","presensi_cloud");
												// Check connection
												if (mysqli_connect_errno())
												{
												echo "Failed to connect to MySQL: " . mysqli_connect_error();
												}
												$jur = $_POST['namajurusan'];
												$akses = $_POST['hakakses'];
												$separat = explode(" ", $akses);
												$oprtor = $separat['1'];
												$val = $separat['2'];
												$fieldss = $separat['0'];

												$preres = mysqli_query($con, "SELECT id FROM jurusanss WHERE nama = '".$jur."'");
												$row = $preres->fetch_assoc();
												$selid = $row['id'];
												$selectedSchema = "presensi_cloud_".$selid;
												$con2 = mysqli_connect("localhost","root","",$selectedSchema);
												if($oprtor == "ALL" && $val == "ALL" && $fieldss=="ALL"){
													$result = mysqli_query($con2,"SELECT mahasiswas.id AS idMahasiswa, mahasiswas.nama AS namaMahasiswa, matakuliahs.nama AS namaMatkul, DATE(kehadirans.tanggal) AS tanggalKuliah, kehadirans.keterangan AS keteranganHadir FROM mahasiswas INNER JOIN kehadirans ON mahasiswas.id = kehadirans.mahasiswas_id INNER JOIN jadwal_matakuliahs ON kehadirans.matakuliahs_id = jadwal_matakuliahs.matakuliahs_id INNER JOIN matakuliahs_kp ON jadwal_matakuliahs.matakuliahs_id = matakuliahs_kp.matakuliahs_id INNER JOIN matakuliahs ON matakuliahs_kp.matakuliahs_id = matakuliahs.id");
												} else{
													$result = mysqli_query($con2,"SELECT mahasiswas.id AS idMahasiswa, mahasiswas.nama AS namaMahasiswa, matakuliahs.nama AS namaMatkul, DATE(kehadirans.tanggal) AS tanggalKuliah, kehadirans.keterangan AS keteranganHadir FROM mahasiswas INNER JOIN kehadirans ON mahasiswas.id = kehadirans.mahasiswas_id INNER JOIN jadwal_matakuliahs ON kehadirans.matakuliahs_id = jadwal_matakuliahs.matakuliahs_id INNER JOIN matakuliahs_kp ON jadwal_matakuliahs.matakuliahs_id = matakuliahs_kp.matakuliahs_id INNER JOIN matakuliahs ON matakuliahs_kp.matakuliahs_id = matakuliahs.id WHERE kehadirans.".$fieldss." = '".$val."'");
												}
												
												
												echo "<label class='col-md-3 control-label' for='inputDefault'>Nama Jurusan : ".$jur."</label>";
												echo "<label class='col-md-3 control-label' for='inputDefault'>Filter : ".$fieldss." ".$oprtor." ".$val."</label>";
												echo "<table class='table table-hover mb-none'>
												<thead>
												<tr>
												<th>Id Mahasiswa</th>
												<th>Nama Mahasiswa</th>
												<th>Nama Matkul</th>
												<th>Tanggal Kuliah</th>
												<th>Keterangan Hadir</th>
												</tr>
												</thead>
												<tbody>";

												if($result){
													while($row = mysqli_fetch_array($result))
													{
													echo "<tr>";
													echo "<td>" . $row['idMahasiswa'] . "</td>";
													echo "<td>" . $row['namaMahasiswa'] . "</td>";
													echo "<td>" . $row['namaMatkul']. "</td>";
													echo "<td>" . $row['tanggalKuliah']. "</td>";
													echo "<td>" . $row['keteranganHadir']. "</td>";
													echo "</tr>";
													}
													echo "</tbody>
													</table>";

													mysqli_close($con);
													}
													else{
														echo "tidak ada data presensi";
													}
											}
												
											?>
								</div>
							</section>
						</div>
					</div>			
				
										<!-- <div class="form-group">
										
											<label class="col-md-3 control-label" for="inputSuccess">Nama Tabel</label>
											<div class="col-md-6">
												<select class="form-control mb-md" name="namaTabel">
													<option value="mahasiswas">Mahasiswa</option>
													<option value="matakuliahs">Matakuliah</option>
													<option value='matakuliahs_buka'>Matakuliah Dibuka</option>
													<option value='kehadirans'>kehadiran</option>
													<option value="jadwals">Jadwal</option>
												</select>	
											</div>
										</div>
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Nama Field</label>
											<div class="col-md-6">
												<input type="text" class="form-control" id="inputDefault" name="namaField">
											</div>				
										</div>
										<div class="form-group">
										
											<label class="col-md-3 control-label" for="inputSuccess">Value</label>
											<div class="col-md-6">
												<select class="form-control mb-md" name="value">
													<option value="INTEGER">Number</option>
													<option value="VARCHAR">Text</option>
													<option value="DATETIME">Date Time</option>
													<option value="BLOB">Gambar</option>
												</select>	
											</div>
										</div>
										
										<div class="form-group">
											<label class="col-md-3 control-label" for="inputDefault">Length</label>
											<div class="col-md-6">
												<input type="number" class="form-control" id="inputDefault" name="length">
											</div>	
											<span class="input-group-btn">
													<input type="submit" class="btn btn-success" value="Add">
											</span>
										</div>
									</form>
								</div>
							</section>
						</div>
					</div> -->

						
					<!-- end: page -->
				</section>
			</div>

			<aside id="sidebar-right" class="sidebar-right">
				<div class="nano">
					<div class="nano-content">
						<a href="#" class="mobile-close visible-xs">
							Collapse <i class="fa fa-chevron-right"></i>
						</a>
			
						<div class="sidebar-right-wrapper">
			
							<div class="sidebar-widget widget-calendar">
								<h6>Current Date</h6>
								<div data-plugin-datepicker data-plugin-skin="dark" ></div>
							</div>
						</div>
					</div>
				</div>
			</aside>
		</section>

		<!-- Vendor -->
		<script src="assets/vendor/jquery/jquery.js"></script>
		<script src="assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="assets/vendor/bootstrap/js/bootstrap.js"></script>
		<script src="assets/vendor/nanoscroller/nanoscroller.js"></script>
		<script src="assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="assets/vendor/magnific-popup/magnific-popup.js"></script>
		<script src="assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Specific Page Vendor -->
		<script src="assets/vendor/jquery-autosize/jquery.autosize.js"></script>
		<script src="assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="assets/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="assets/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="assets/javascripts/theme.init.js"></script>
	</body>
</html>