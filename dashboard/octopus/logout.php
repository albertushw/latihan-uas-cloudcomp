<?php
session_start();

unset($_SESSION['username']);
unset($_SESSION['type_user']);
session_destroy();

header("location: ../../loginss.php");
?>